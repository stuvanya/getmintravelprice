using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        private static int GetMinTravelPrice(int[][] cells)
        {
            //множество стоимостей путей из начальной в конечную точку поля
            var values = new SortedSet<int>();
            NextStep(cells, 0, 0, cells.Length, cells[0].Length, 0, 0, new SortedSet<Tuple<int, int>>(), values);
            //возвращаем минимальную цену пути
            return values.ElementAt<int>(0);
        }

        //рекурсивная функция следующего шага, добавляет значение в values в случае достижения конечной точки поля
        private static void NextStep(int[][] cells, int x, int y, int m, int n, int cur_price, int prev_cell_price, SortedSet<Tuple<int, int>> visited_cells, SortedSet<int> values)
        {
            //нашли бесплатный путь, дальше нет смысла искать другие пути
            //if (values.Contains(0))
            //    return;
            //нет смысла, так как следующая проверка тоже детектирует данный случай

            //если если уже найден путь с ценой меньшей или равной цене текущего пути, то дальше нет смысла продолжать
            if (values.Count > 0)
                if (cur_price >= values.ElementAt<int>(0))
                    return;

            //увеличиваем цену пути на 1, если в новой клетке поменялось значение
            if (prev_cell_price != cells[y][x] && !(x == 0 && y == 0))
                cur_price += 1;

            //добавляем текущую клетку в уже посещенные, дабы избежать зацикливаний
            visited_cells.Add(Tuple.Create(x, y));

            //дошли до целевой точки
            if (x == n - 1 && y == m - 1)
            {
                values.Add(cur_price);
                return;
            }

            SortedSet<Tuple<int, int>> new_visited_cells = new SortedSet<Tuple<int, int>>(visited_cells);
            //шаг вверх
            if (y != 0 && !visited_cells.Contains(Tuple.Create(x, y - 1)))
                NextStep(cells, x, y - 1, m, n, cur_price, cells[y][x], new_visited_cells, values);

            //шаг вниз
            if (y != m - 1 && !visited_cells.Contains(Tuple.Create(x, y + 1)))
                NextStep(cells, x, y + 1, m, n, cur_price, cells[y][x], new_visited_cells, values);

            //шаг влево
            if (x != 0 && !visited_cells.Contains(Tuple.Create(x - 1, y)))
                NextStep(cells, x - 1, y, m, n, cur_price, cells[y][x], new_visited_cells, values);

            //шаг вправо
            if (x != n - 1 && !visited_cells.Contains(Tuple.Create(x + 1, y)))
                NextStep(cells, x + 1, y, m, n, cur_price, cells[y][x], new_visited_cells, values);
        }

        static void Main(string[] args)
        {
            /*int[][] cells = new int[][] {
                 new int[]{ 0, 0, 1 },
                 new int[]{ 0, 1, 0 },
                 new int[]{ 0, 1, 1 }
            };*/



            /*int[][] cells = new int[][] {
                 new int[]{ 0 }
            };*/

            /*int[][] cells = new int[][] {
                 new int[]{ 0, 0, 1 },
                 new int[]{ 1, 0, 0 },
                 new int[]{ 1, 1, 0 }
            };*/


            /*int[][] cells = new int[][] {
                 new int[]{ 0, 0, 1 },
                 new int[]{ 1, 0, 1 },
                 new int[]{ 0, 0, 1 },
                 new int[]{ 0, 1, 1 },
                 new int[]{ 0, 0, 0 }
            };*/

            int[][] cells = new int[][] {
                 new int[]{ 0, 1, 0, 0, 0, 0 },
                 new int[]{ 0, 1, 0, 1, 1, 0 },
                 new int[]{ 0, 1, 0, 0, 1, 0 },
                 new int[]{ 0, 1, 1, 0, 1, 0 },
                 new int[]{ 0, 0, 0, 0, 1, 0 }
            };

            Console.WriteLine(GetMinTravelPrice(cells));
        }
    }
}
